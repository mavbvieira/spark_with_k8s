# Apache Spark [Operator] on Kubernetes [K8S] - [spark-on-k8s]

> https://spark.apache.org/docs/latest/structured-streaming-programming-guide.html#monitoring-streaming-queries  
> https://spark.apache.org/docs/latest/structured-streaming-kafka-ikgpntegration.html  
> https://spark.apache.org/docs/latest/sql-data-sources-jdbc.html  

### local jars for integration
```sh
# libraries to copy to jars folder
# local environment [copy]
# [jars loc] = /usr/local/lib/python3.9/site-packages/pyspark/jars

cp spark-streaming-kafka-0-10_2.12-3.0.1.jar /usr/local/lib/python3.9/site-packages/pyspark/jars
cp spark-streaming_2.12-3.0.1 /usr/local/lib/python3.9/site-packages/pyspark/jars
```

### verify apache kafka resource
```sh
# generate events into input topics
curl -i http://localhost:5000/kafka/users-json\?broker\=20.186.81.172:9094\&topic\='src-app-users-json'\&events\=1000
curl -i http://localhost:5000/kafka/movies-titles-json\?broker\=20.186.81.172:9094\&topic\='src-app-movies-titles-data-json'\&events\=1000

# verify data written into [input] and [output] topic
kafkacat -C -b $KAFKABROKER -t src-app-users-json -J -o end
kafkacat -C -b $KAFKABROKER -t src-app-movies-titles-data-json -J -o end
```

### containerize [spark] app
```sh
# location = /Users/luanmorenomaciel/BitBucket/applications/k8s-spark-operator/pr-movies-analysis

# app name = pr-movies-analysis
# location of dockerfile [build image]
Dockerfile

# build image
# tag image
# push image to registry
docker build . -t pr-movies-analysis:3.0.0
docker tag pr-movies-analysis:3.0.0 owshq/pr-movies-analysis:3.0.0
docker push owshq/pr-movies-analysis:3.0.0
```

### install and configure spark operator [spark-on-k8s]
```sh
# select cluster to deploy
kubectx aks-owshq-orion-dev
k get nodes

# install spark operator
helm repo update
helm install spark spark-operator/spark-operator --namespace processing
helm ls -n processing
kgp -n processing

# create & verify cluster role binding perms
k apply -f /Users/luanmorenomaciel/BitBucket/applications/k8s-spark-operator/pr-movies-analysis/crb-spark-operator-processing.yaml -n processing
k describe clusterrolebinding crb-spark-operator-processing

# deploy spark application [kubectl] for testing purposes
# eventually trigger this application using airflow
# [deploy application]
kubens processing
k apply -f /Users/luanmorenomaciel/BitBucket/applications/k8s-spark-operator/pr-movies-analysis/batch-pr-movies-analysis-py.yaml -n processing
k apply -f /Users/luanmorenomaciel/BitBucket/applications/k8s-spark-operator/pr-movies-analysis/stream-pr-movies-analysis-py.yaml -n processing

# get yaml detailed info
# verify submit
k get sparkapplications -n processing
k get sparkapplications batch-pr-movies-analysis-py -o=yaml -n processing
k get sparkapplications stream-pr-movies-analysis-py -o=yaml -n processing
k describe sparkapplication batch-pr-movies-analysis-py -n processing
k describe sparkapplication stream-pr-movies-analysis-py -n processing

# verify logs in real-time
k logs batch-pr-movies-analysis-py-driver
k logs stream-pr-movies-analysis-py-driver

# port forward to spark ui
POD=batch-pr-movies-analysis-py-driver
POD=stream-pr-movies-analysis-py-driver
k port-forward $POD 4040:4040

# housekeeping
k delete SparkApplication batch-pr-movies-analysis-py -n processing
k delete SparkApplication stream-pr-movies-analysis-py -n processing
k delete clusterrolebinding crb-spark-operator-processing
helm uninstall spark -n processing
```
