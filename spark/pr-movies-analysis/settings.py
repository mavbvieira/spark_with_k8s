# import libraries
import os

# get [env] information
BOOTSTRAP_SERVERS = 'edh-kafka-brokers.ingestion.svc.Cluster.local:9092'
JDBC_POSTGRES_URL = 'jdbc:postgresql://yb-tservers.database.svc.Cluster.local:5433/owshq'
INPUT_USERS_TOPIC = 'src-app-users-json'
INPUT_MOVIES_TITLES_TOPIC = 'src-app-movies-titles-data-json'
OUTPUT_TOPIC_COUNTS_PER_COUNTRY_BATCH = 'output-pyspark-counts-country-batch-json'
OUTPUT_TOPIC_COUNTS_PER_COUNTRY_STREAM = 'output-pyspark-counts-country-stream-json'
STARTING_OFFSETS = 'latest'
CHECKPOINT = 'checkpoint'

# submit args
# os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.1,org.postgresql:postgresql:42.2.19 pyspark-shell'
os.environ['PYSPARK_SUBMIT_ARGS'] = '--driver-class-path /Users/luanmorenomaciel/BitBucket/applications/k8s-spark-operator/pr-movies-analysis/jars/postgresql-42.2.19.jar --jars /Users/luanmorenomaciel/BitBucket/applications/k8s-spark-operator/pr-movies-analysis/jars/postgresql-42.2.19.jar pyspark-shell'
