# import libraries
import time
from settings import *
from os.path import abspath
from pyspark.sql.functions import *
from pyspark.sql import SparkSession
from schemas import jsc_users, jsc_movies_titles

# set default location for [spark-warehouse]
warehouse_location = abspath('spark-warehouse')

# main spark program
if __name__ == '__main__':

    # init spark session
    spark = SparkSession \
            .builder \
            .appName("pr-movies-analysis-stream") \
            .config("spark.jars.packages", "org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.1") \
            .config("spark.sql.warehouse.dir", warehouse_location) \
            .enableHiveSupport() \
            .getOrCreate()

    # set log level to info
    # [INFO] or [WARN] for more detailed logging info
    spark.sparkContext.setLogLevel("WARN")

    # event schema message [json]
    # refer to schemas.py file
    # set desired timestamp output
    sch_users = jsc_users
    sch_movies_titles = jsc_movies_titles
    jsonOptions = {"timestampFormat": "yyyy-MM-dd'T'HH:mm:ss.sss'Z'"}

    # read variables
    # print env variables from settings.py
    print(BOOTSTRAP_SERVERS)
    print(INPUT_USERS_TOPIC)
    print(INPUT_MOVIES_TITLES_TOPIC)
    print(OUTPUT_TOPIC_COUNTS_PER_COUNTRY_STREAM)
    print(STARTING_OFFSETS)

    # read events from [apache kafka]
    # using readStream coming from apache kafka
    # json output
    stream_users = spark \
        .readStream \
        .format("kafka") \
        .option("kafka.bootstrap.servers", BOOTSTRAP_SERVERS) \
        .option("subscribe", INPUT_USERS_TOPIC) \
        .option("startingOffsets", STARTING_OFFSETS) \
        .option("checkpoint", CHECKPOINT) \
        .load() \
        .select(from_json(col("value").cast("string"), sch_users, jsonOptions).alias("users"))

    stream_movies_titles = spark \
        .readStream \
        .format("kafka") \
        .option("kafka.bootstrap.servers", BOOTSTRAP_SERVERS) \
        .option("subscribe", INPUT_MOVIES_TITLES_TOPIC) \
        .option("startingOffsets", STARTING_OFFSETS) \
        .option("checkpoint", CHECKPOINT) \
        .load() \
        .select(from_json(col("value").cast("string"), sch_movies_titles, jsonOptions).alias("movies_titles"))

    # select columns [for processing]
    # stateless transformation [does not require info from previous rows]
    # select, explode, map, flatmap, filter, where
    # applied both in batch and stream operations
    get_columns_for_stream_users = stream_users.select(
        col("users.user_id").alias("users_user_id"),
        col("users.first_name"),
        col("users.last_name"),
        col("users.country"),
        col("users.job"),
        col("users.dt_current_timestamp").alias("users_event_time")
    )

    get_columns_for_stream_movies_titles = stream_movies_titles.select(
        col("movies_titles.user_id").alias("movies_user_id"),
        col("movies_titles.genres"),
        col("movies_titles.original_title"),
        col("movies_titles.release_date"),
        col("movies_titles.status"),
        col("movies_titles.dt_current_timestamp").alias("movies_event_time")
    )

    # view raw structured streaming dataframes
    # console [output] = users and titles
    """
    query_users = get_columns_for_stream_users \
        .writeStream \
        .format("console") \
        .start()
    print(query_users)

    query_movie_titles = get_columns_for_stream_movies_titles \
        .writeStream \
        .format("console") \
        .start()
    print(query_movie_titles)
    """

    # stream-stream join - micro-batches
    # inner join applied - stream-stream [joins] using structured streaming
    # groupBy = out of order, and may be arbitrarily delayed
    # 1 = received on same micro-batch = computed
    # 2 = earlier or late arrival ~ without watermark no way to distinguish in which timeframe
    # withWatermark = delays and window to consider events
    # write into output console [view]
    stream_watermark_users = get_columns_for_stream_users.withWatermark("users_event_time", "1 minute")
    stream_watermark_movies_titles = get_columns_for_stream_movies_titles.withWatermark("movies_event_time", "1 minute")
    stream_join_users_titles = stream_watermark_users.join(stream_watermark_movies_titles, expr("""users_user_id = movies_user_id"""))

    query_join_dt_watermark = stream_join_users_titles \
        .writeStream \
        .format("console") \
        .start()
    print(query_join_dt_watermark)

    # [0] = count of views per country by user [stateful]
    # aggregation based with event-time windows
    # console and apache kafka topic output
    get_counts_per_country = stream_join_users_titles \
        .groupBy("country", "users_event_time") \
        .count()

    query_grouped_streams = get_counts_per_country \
        .writeStream \
        .format("console") \
        .start()
    print(query_grouped_streams)

    # monitoring streaming queries
    # structured streaming output info
    # read last progress
    # last status of query
    print(query_grouped_streams.lastProgress)
    print(query_grouped_streams.status)

    # block until query is terminated
    # query_users.awaitTermination()
    # query_movie_titles.awaitTermination()
    query_grouped_streams.awaitTermination()

    """
    # trigger using processing time = interval of the micro-batches
    # formatting to deliver to apache kafka payload (value)
    write_into_topic_counts_per_country = get_counts_per_country \
        .select("country", "users_event_time", "count") \
        .select(to_json(struct(col("country"), col("users_event_time").alias("event_time"), col("count"))).alias("value")) \
        .writeStream \
        .format("kafka") \
        .option("kafka.bootstrap.servers", BOOTSTRAP_SERVERS) \
        .option("topic", OUTPUT_TOPIC_COUNTS_PER_COUNTRY_STREAM) \
        .option("checkpointLocation", CHECKPOINT) \
        .outputMode("append") \
        .trigger(processingTime="15 seconds") \
        .start()
        
    # monitoring streaming queries
    # structured streaming output info
    # read last progress
    # last status of query
    print(write_into_topic_counts_per_country.lastProgress)
    print(write_into_topic_counts_per_country.status)

    # block until query is terminated
    # query_users.awaitTermination()
    # query_movie_titles.awaitTermination()
    write_into_topic_counts_per_country.awaitTermination()
    """

